const BaseService = require('./base.service')
const http = require('../lib/https')

module.exports = class Vehicles extends BaseService{

    /**
     * 建構子
     * @param apiKey
     */
    constructor(apiKey){
        super('vehicles', apiKey)

        this.errorCode = [
            {
                code: 1000,
                errorMsg: '參數錯誤'
            },
            {
                code: 1001,
                errorMsg: 'API Key 錯誤'
            },
            {
                code: 1003,
                errorMsg: '此 Vehicle 不存在'
            }
        ]
    }

    /**
     *
     * @returns {Promise<string>}
     */
    viewOne(name) {
        let postData = [
            name
        ]

        return http.post(
            this.BaseUrl + '/view.json',
            postData,
            this.RequestOption)
            .then(response => {
                console.log("response: ", response)
                return {
                    status: true,
                    data: response.results[0].vehicle
                }
            })
            .catch(err => {
                if (err[0].errors) {
                    let error = this.errorCode.find(item => {
                        return item.code === err[0].errors[0].code
                    })
                    return {
                        status: false,
                        data: error.errorMsg
                    }
                } else {
                    return {
                        status: false,
                        data: err
                    }
                }
            })
    }

    /**
     *
     * @returns {Promise<string>}
     */
    viewAll() {
        let postData = []

        return http.post(
            this.BaseUrl + '/view/all.json',
            postData,
            this.RequestOption)
            .then(response => {
                return {
                    status: true,
                    data: response.vehicles
                }
            })
            .catch(err => {
                if (err.code) {
                    let error = this.errorCode.find(item => {
                        return item.code === err.code
                    })
                    return {
                        status: false,
                        data: error.errorMsg
                    }
                } else {
                    return {
                        status: false,
                        data: err
                    }
                }
            })
    }
}
