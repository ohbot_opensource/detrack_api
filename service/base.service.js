
module.exports = class BaseService {

    /**
     * 建構子
     * @param type
     * @param apiKey
     */
    constructor(type, apiKey) {

        /**
         * 基本的URL根據不同的Service提供不同URL
         */
        this.BaseUrl = 'https://app.detrack.com/api/v1/' + type

        /**
         * 基本的RequestOption根據不同的Service提供不同RequestOption
         */
        this.RequestOption = {
            'Content-Type': 'application/json',
            'X-API-KEY': apiKey
        }

        /**
         * 基本的APIKey
         */
        this.ApiKey = apiKey
    }

    /**
     * 將參數變成Array
     * @param data
     * @returns {*[]}
     */
    setArray(...data) {
        return data
    }
};
