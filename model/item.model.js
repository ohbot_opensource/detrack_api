module.exports.Item = class Item {

    constructor(sku, description, quantity){
        /**
         * 商品sku
         * Stock keeping unit or item #.
         */
        this.sku = sku

        /**
         * 商品敘述
         * The item description.
         */
        this.desc = description

        /**
         * 商品數量
         * Quantity of the item to be delivered.
         */
        this.qty = quantity
    }
}

module.exports.NotifyItem = class NotifyItem {

    constructor(sku, description, quantity, reject, reason){
        /**
         * 商品sku
         * Stock keeping unit or item #.
         */
        this.sku = sku

        /**
         * 商品敘述
         * The item description.
         */
        this.desc = description

        /**
         * 商品數量
         * Quantity of the item to be delivered.
         */
        this.qty = quantity

        /**
         * 退回數量
         * Quantity of the item rejected by recipient.
         */
        this.reject = reject

        /**
         * 原因
         * Reason of item rejection.
         */
        this.reason = reason
    }
}
