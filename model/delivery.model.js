module.exports.Delivery = class Delivery {

    constructor(date, uuid, address, items = []){
        /**
         * 運送貨物時間
         * The collection date. Format: YYYY-MM-DD e.g. 2014-02-28.
         */
        this.date = date

        /**
         * 運送貨物的UUId
         * The D.O. #. This field must be unique for the date.
         */
        this.do = uuid

        /**
         * 運送貨物地址
         * The collection address. Always include country name for accurate geocoding results.
         */
        this.address = address

        /**
         * 運送來源：時間
         * The delivery time window. This will be displayed in the job list view and the delivery detail view on the app.
         */
        this.delivery_time = ''

        /**
         * 運送來源：名字
         * The name of the recipient to deliver to. This can be a person’s name e.g. John Tan, a company’s name e.g. ABC Inc., or both e.g. John Tan (ABC Inc.)
         */
        this.deliver_to = ''

        /**
         * 顧客的電話號碼（可以顯示在App，送貨員可以打電話給此顧客）
         * The phone number of the sender. If specified, the driver can call the sender directly from the app.
         * @type {string}
         */
        this.phone = ''

        /**
         * 完成後回傳email
         * @type {string}
         */
        this.notify_email = ''

        /**
         * 完成後回傳url
         * @type {string}
         */
        this.notify_url = ''

        /**
         * Assign的人員：名字
         * @type {string}
         */
        this.assign_to = ''

        /**
         * 其他指示、備註
         * @type {string}
         */
        this.instructions = ''

        /**
         * 地區、區域
         * @type {string}
         */
        this.zone = ''

        /**
         *
         * @type {Array}
         */
        this.items = items
    }
}

module.exports.DeliveryNotify = class DeliveryNotify {

    constructor(date, uuid, address, items = []){
        /**
         * 運送貨物時間
         * The collection date. Format: YYYY-MM-DD e.g. 2014-02-28.
         */
        this.date = date

        /**
         * 運送貨物的UUId
         * The D.O. #. This field must be unique for the date.
         */
        this.do = uuid

        /**
         * 運送貨物地址
         * The collection address. Always include country name for accurate geocoding results.
         */
        this.address = address

        /**
         * 運送來源：時間
         * The delivery time window. This will be displayed in the job list view and the delivery detail view on the app.
         */
        this.delivery_time = ''

        /**
         * 運送來源：名字
         * The name of the recipient to deliver to. This can be a person’s name e.g. John Tan, a company’s name e.g. ABC Inc., or both e.g. John Tan (ABC Inc.)
         */
        this.deliver_to = ''

        /**
         * 顧客的電話號碼（可以顯示在App，送貨員可以打電話給此顧客）
         * The phone number of the sender. If specified, the driver can call the sender directly from the app.
         * @type {string}
         */
        this.phone = ''

        /**
         * 完成後回傳email
         * @type {string}
         */
        this.notify_email = ''

        /**
         * 完成後回傳url
         * @type {string}
         */
        this.notify_url = ''

        /**
         * Assign的人員：名字
         * @type {string}
         */
        this.assign_to = ''

        /**
         * 其他指示、備註
         * @type {string}
         */
        this.instructions = ''

        /**
         * 地區、區域
         * @type {string}
         */
        this.zone = ''

        /**
         *
         * @type {string}
         */
        this.reason = ''

        /**
         *
         * @type {string}
         */
        this.note = ''

        /**
         *
         * @type {string}
         */
        this.received_by = ''

        /**
         *
         * @type {string}
         */
        this.image = ''

        /**
         *
         * @type {string}
         */
        this.view_image_url = ''

        /**
         *
         * @type {string}
         */
        this.time = ''

        /**
         *
         * @type {string}
         */
        this.pod_lat = ''

        /**
         *
         * @type {string}
         */
        this.pod_lng = ''

        /**
         *
         * @type {string}
         */
        this.pod_address = ''

        /**
         *
         * @type {Array}
         */
        this.items = items
    }
}
